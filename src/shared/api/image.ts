import axios from 'axios'

const url = 'https://pixel.zufa.ru/api/'
const options = {
    headers: {
        'Content-Type': 'multipart/form-data',
        // 'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        // 'Access-Control-Allow-Headers': 'Content-Type, X-Auth-Token, Origin, Authorization'
    }
}

export function setImage(data: FormData) {
    data.append('method', 'setImage')

    return axios.post(url, data, options)
}

export function cutOutImage(data: FormData) {
    data.append('method', 'cutOutImage')

    return axios.post(url, data, options)
}

export function sendEmail(data: FormData) {
    data.append('method', 'sendEmail')

    return axios.post(url, data, options)
}