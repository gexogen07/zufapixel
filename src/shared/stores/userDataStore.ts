import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useUserDataStore = defineStore('user', () => {
    const code = ref('')
    const codeIsError = ref(false)
    const errorText = ref('')
    const numberCodeRepeat = ref(0)

    const file = ref('')
    const email = ref('')

    function setCode(value: string) {
        code.value = value
    }

    function setCodeIsError(value: boolean, text: string) {
        codeIsError.value = value
        errorText.value = text
    }

    function setEmail(value: string) {
        email.value = value
    }

    function setFile(value: string) {
        file.value = value
    }

    function setNumberCodeRepeat(value: number) {
        numberCodeRepeat.value = value
    }

    return {
        code,
        codeIsError,
        errorText,
        numberCodeRepeat,
        email,
        file,
        setCode,
        setCodeIsError,
        setEmail,
        setFile,
        setNumberCodeRepeat
    }
})