import { defineStore } from 'pinia'
import { reactive, ref } from 'vue'

interface Image {
    id: number
    pic: string
}

export const useImgStore = defineStore('img', () => {
    const step = ref<number>(1)
    const cutOutBg = ref<string>('0')
    const gridSize = ref<string>('0')
    const img = ref<HTMLImageElement>()
    const images = ref<Image[]>([])

    const selectingImage = reactive<Image>({
        id: 0,
        pic: ''
    })

    function setGridSize(value: string) {
        gridSize.value = value
    }

    function setImg(value: HTMLImageElement) {
        img.value = value
    }

    function setSelectingImg(img: Image) {
        selectingImage.id = img.id
        selectingImage.pic = img.pic
    }

    function setImages(value: Image[]) {
        images.value = value
    }

    function prevStep() {
        step.value--
    }

    function nextStep() {
        step.value++
    }

    function setCutOutBg(value: boolean) {
        cutOutBg.value = value ? '1' : '0';
    }

    return {
        step,
        gridSize,
        img,
        cutOutBg,
        selectingImage,
        images,
        setGridSize,
        setImg,
        setImages,
        setSelectingImg,
        prevStep,
        nextStep,
        setCutOutBg
    }
})