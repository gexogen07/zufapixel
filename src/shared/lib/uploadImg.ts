export function uploadImg(file: File, successCall: Function, errorCall?: Function) {
    if (file.size > 20971520) {
        if (errorCall) errorCall()
        return
    }

    const fr = new FileReader()
    fr.onload = () => {
        const img = new Image()
        img.onload = () => successCall(img)
        img.onerror = () => {
            if (errorCall) errorCall()
        }
        img.src = fr.result as string
    }
    fr.onerror = () => {
        if (errorCall) errorCall()
    }
    fr.readAsDataURL(file)
}