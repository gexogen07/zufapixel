<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');
header('Content-Type: application/json; charset=utf-8');

include '../DatabaseManager.php';
include 'api.php';
include '../config.php';

$api = new Api();

$data = file_get_contents('php://input');
$data = json_decode($data, true);

try {
    if(!empty($_FILES) ) {
        if( array_key_exists('method',$_REQUEST) ){
            switch ($_REQUEST['method']) {
                case 'setImage':
                    echo json_encode($api->setImage($_FILES));
                    break;
            }
        }
    } else {
        if( array_key_exists('method',$_REQUEST) ){
            switch ($_REQUEST['method']) {
                case 'cutOutImage':
                    echo json_encode($api->cutOutImage($_REQUEST));
                    break;
                case 'setImage':
                    echo json_encode($api->setImage($_REQUEST));
                    break;
                case 'sendEmail':
                    echo json_encode($api->sendEmail($_REQUEST));
                    break;
            }
        }else if( array_key_exists('method',$data) ){
            switch ($data['method']) {
                case 'cutOutImage':
                    echo json_encode($api->cutOutImage($data));
                    break;
                case 'setImage':
                    echo json_encode($api->setImage($data));
                    break;
                case 'sendEmail':
                    echo json_encode($api->sendEmail($data));
                    break;
            }
        }
    }
} catch (\Exception $e) {
    // Проверяем, включен ли режим разработчика,
    // если да то показываем причину ошибки,
    // если нет то показываем Server Error
    $message = true === config()['dev_mode'] ? $e->getMessage() : 'Server error';

    http_response_code(500);

    echo json_encode([
        'success' => false,
        'text' => $message
    ]);
}
