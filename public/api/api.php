<?php

include '../pdf/FPDF/tfpdf/tfpdf.php';
include '../mail/mail.php';

class Api
{
    const COLOR_A_LILAC = [140, 143, 174];
    const COLOR_B_VIOLET = [88, 69, 99];
    const COLOR_C_MAGENTA = [66, 34, 60];
    const COLOR_D_YELLOW = [245, 237, 186];
    const COLOR_E_CORAL = [215, 155, 125];
    const COLOR_F_BROWN = [154, 99, 72];
    const COLOR_G_RED = [157, 48, 59];
    const COLOR_H_GREEN = [100, 125, 52];
    const COLOR_I_LIGHTBLUE = [126, 196, 193];
    const COLOR_J_CYAN = [23, 67, 75];

    const GENERATION_ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    private $colors = [];

    public $arPalette = [
        [51,51,51],
        [57,57,57],
        [86,86,86],
        [91,91,91],
        [96,96,96],
        [110,110,110],
        [148,148,148],
        [167,167,167],
        [172,172,172],
        [220,220,220]
    ];
    public $arPaletteColor = [
        self::COLOR_J_CYAN, //J
        self::COLOR_C_MAGENTA, //C
        self::COLOR_B_VIOLET, //B
        self::COLOR_H_GREEN, //H
        self::COLOR_G_RED, //G
        self::COLOR_F_BROWN, //F
        self::COLOR_D_YELLOW, //A - D
        self::COLOR_E_CORAL, //E
        self::COLOR_I_LIGHTBLUE, //I
        self::COLOR_A_LILAC //D - A
    ];
    public $arPaletteNumber = [ 'J', 'C', 'B', 'H', 'G', 'F', 'D', 'E', 'I', 'A' ];
    public $grid_size = 0;
    private $uploadExtension = '.jpg';
    private $cutOutFlow = false;

    private static function generateRandomString($length = 10): string {
        $randomString = '';
        $len = strlen(self::GENERATION_ALPHABET) - 1;

        for ($i = 0; $i < $length; $i++) {
            $randomString .= self::GENERATION_ALPHABET[random_int(0, $len)];
        }

        return $randomString;
    }

    public function cutOutImage($data): array {
        $config = config();
        $imageName = self::generateRandomString(5).time().'.cutout'; // extension doesn't really matter
        $filePath = @"../upload/$imageName";

        file_put_contents($filePath, file_get_contents($data['pic']));

        // cURL header for file uploading
        $headers = array("APIKEY:".$config['cutout']['api_key']);

        $curlFile = new CURLFile($filePath, 'text/plain', $imageName);

        $ch = curl_init($config['cutout']['url']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'file' => $curlFile,
        ]);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
        $result = curl_exec($ch);

        $response = json_decode($result, true);
        if (
            $response['code'] === 0
            && isset($response['data']['imageBase64'])
            && $response['data']['imageBase64'] != ""
        ) {
            return [
                'success' => true,
                'result' => $response['data']['imageBase64']
            ];
        }

        // we have an error, send mail to us
        $html = "<p>Произошла ошибка при удалении фона. CutOut.pro вернул следующий ответ:</p>".
            "<p>$response</p>";

        sendErrorMail($config['smtp'], $html);

        return [
            'success' => false,
            'result' => "Сервис удаления фона недоступен. Попробуйте позднее"
        ];
    }

    private function analyzeUploadData($data) {
        if ((int)$data['cutOutBg'] === 1) {
            $this->cutOutFlow = true;
            // we have to force png to keep transparency
            $this->uploadExtension = '.png';
            return;
        }

        $this->uploadExtension = '.jpg';
    }

    public function setImage($data) {
        $this->grid_size = (int)$data['grid_size'];
        $this->analyzeUploadData($data);

        $imageName = self::generateRandomString(5).time().$this->uploadExtension;

        file_put_contents("../upload/$imageName", file_get_contents($data['pic']));

        if ( $this->grid_size < 10 ) {
            $this->arPalette = [
                [51,51,51],
                [57,57,57],
                [86,86,86],
                [91,91,91],
                [96,96,96],
                [148,148,148],
                [167,167,167],
                [220,220,220]
            ];
            $this->arPaletteColor = [
                self::COLOR_J_CYAN, //J
                self::COLOR_C_MAGENTA, //C
                self::COLOR_B_VIOLET, //B
                self::COLOR_H_GREEN, //H
                self::COLOR_G_RED, //G
                //F
                self::COLOR_D_YELLOW, //D
                self::COLOR_E_CORAL, //E
                //I
                self::COLOR_A_LILAC //A
            ];
            $this->arPaletteNumber = [ 'J', 'C', 'B', 'H', 'G', 'D', 'E', 'A' ];
        } elseif ( $this->grid_size == 71 ) {
            $this->arPaletteColor = [
                self::COLOR_J_CYAN, //J
                self::COLOR_C_MAGENTA, //C
                //B
                self::COLOR_H_GREEN, //H
                self::COLOR_G_RED, //G
                //F
                //D
                self::COLOR_E_CORAL,//E
                self::COLOR_I_LIGHTBLUE //I
                //A
            ];
            $this->arPaletteNumber = [ 'J', 'C', 'H', 'G', 'E', 'I' ];
        }

        return $this->transformImage($imageName);
    }

    private function transformImage($imageName){
        $nPointW = 9; // Количество блоков пикселей по горизонтали
        $nPointH = 12; // Количество блоков пикселей по вертикали
        if ( $this->grid_size == 71 ) {
            $nPointW = 8;
            $nPointH = 11;
        }
        $nCellW = $this->grid_size; // количество пикселей внутри 1 блока по горизонтали
        $nCellH = $this->grid_size; // количество пикселей внутри 1 блока по вертикали

        if ( $this->grid_size == 71 ) {
            $nCellW = 7;
            $nCellH = 7;
        }

        $nameImg = "../upload/$imageName";

        $typeColor = 'fullcolor'; // gray or fullcolor
        $arThreshold = [
            -30, 0, -40, -15, -50, -20
        ];
        $images = [];

        $jsonFileName = str_replace($this->uploadExtension, '', $imageName);
        $fileRes = fopen("imagesJson/$jsonFileName.json", "x");

        // создание набора изображений
        for( $i = 0; $i < count($arThreshold); $i++ ){
            // создание палитры цветов для каждого изображения
            $arPaletteFullColor = [];
            if( $typeColor === 'fullcolor' ){
                $res = $this->getPaletteFull($arThreshold[$i], $typeColor);
                $arPaletteFull = $res[0];
                $arPaletteFullColor = $res[1];
            } else {
                $arPaletteFull = $this->getPaletteFull($arThreshold[$i], $typeColor);
            }

            // создание изображений
            $im = $this->cutOutFlow ? self::createTransparentPng($nameImg) : imagecreatefromjpeg($nameImg);
            imagefilter($im, IMG_FILTER_CONTRAST, $arThreshold[$i]);
            // отзеркаливание изображения для сборки наклеек
            if ( $this->grid_size == 71 ) {
                imageflip($im, IMG_FLIP_HORIZONTAL);
            }

            /*if ($i <=1 ) {
                imagefilter($im, IMG_FILTER_BRIGHTNESS, 80);
            }*/
            $w = imagesx($im);
            $h = imagesy($im);

            $qPixXPoint = $w/$nCellW/$nPointW;
            $qPixYPoint = $h/$nCellH/$nPointH;

            // превращаем цветное серое пиксельное изображение
            $arColorsNew = [];

            $res = $this->getPicOne($w, $h, $im, $arColorsNew, $arPaletteFull);
            $arColorsNew = $res['colors'];

            $newImageName = str_replace($this->uploadExtension, '', $imageName).'_new'.($i+1);

            $pic = config()['app_url'].'/upload/'.$this->showImg($jsonFileName, $newImageName, $nPointH, $nCellH, $nCellW, $nPointW, $w, $h, $qPixXPoint, $qPixYPoint, $arColorsNew, $typeColor, $arPaletteFull, $arPaletteFullColor);

            if( $typeColor === 'fullcolor' ){
                $images[] = [
                    'id' => $i,
                    'pic' => $pic,
                    'tmp'=> 'arColorsNew'
                ];
            }

        }

        fclose($fileRes);
        return $images;
    }

    private function getPixelImg($iPoint, $jPoint, $qPixXPoint, $qPixYPoint, $arColorsNew, $typeColor) {
        $startI = ceil($iPoint*$qPixYPoint);
        $startJ = ceil($jPoint*$qPixXPoint);

        $endI = ceil($startI+$qPixYPoint);
        $endJ = ceil($startJ+$qPixXPoint);

        $quantity = 0;
        $groupAlpha = 0; // fully opaque

        $sum_r = $sum_g = $sum_b = 0;
        for( $i1 = $startI; $i1 <= $endI; $i1++ ) {
            for( $j1 = $startJ; $j1 <= $endJ; $j1++ ) {
                if ( $arColorsNew[$j1][$i1] ) {

                    if ($this->cutOutFlow) {
                        $alpha = ($arColorsNew[$j1][$i1] & 0x7F000000) >> 24;
                        $r = ($arColorsNew[$j1][$i1] >> 16) & 0xFF;
                        $g = ($arColorsNew[$j1][$i1] >> 8) & 0xFF;
                        $b = $arColorsNew[$j1][$i1] & 0xFF;
                        $groupAlpha += $alpha;
                    } else {
                        $r = ($arColorsNew[$j1][$i1] >> 16) & 0xFF;
                        $g = ($arColorsNew[$j1][$i1] >> 8) & 0xFF;
                        $b = $arColorsNew[$j1][$i1] & 0xFF;
                    }

                    $sum_r += $r;
                    $sum_g += $g;
                    $sum_b += $b;
                    $quantity++;
                }
            }
            break;
        }

        // средний цвет
        $ceil_r = 0;
        $ceil_g = 0;
        $ceil_b = 0;

        // mean alpha
        $mean_alpha = 0;

        if ($quantity > 0) {
            $ceil_r = ceil($sum_r / $quantity);
            $ceil_g = ceil($sum_g / $quantity);
            $ceil_b = ceil($sum_b / $quantity);
            $mean_alpha = $groupAlpha / $quantity;
        }
        $paletteItem = 0;
        $min = 0;
        $min_key = 0;
        if( $typeColor === 'fullcolor' ) {
            foreach ($this->arPaletteColor as $id => $rgb ) {

                // Дистанция до ближайшего цвета по корню квадратов
                //$distance = sqrt(($ceil_r - $rgb[0])^2 + ($ceil_g - $rgb[1])^2 + ($ceil_b - $rgb[2])^2 );

                //разница цветов по яркости
                $distance = $this->luminanceDistance( [$ceil_r, $ceil_g, $ceil_b], $rgb );
                if ( $id == 0 ) {
                    $min = $distance;
                    $min_key = $id;
                } elseif ( $distance < $min ) {
                    $min = $distance;
                    $min_key = $id;
                }
            }

            if ($mean_alpha < config()['alpha_threshold']) {
                // color circle
                return [$this->arPaletteColor[$min_key][0], $this->arPaletteColor[$min_key][1], $this->arPaletteColor[$min_key][2], 0 ];
            } else if ($this->grid_size !== 71) {
                // mirrored template replace
                return [self::COLOR_D_YELLOW[0], self::COLOR_D_YELLOW[1], self::COLOR_D_YELLOW[2], 0];
            } else {
                // transparent circle
                return [0, 0, 0, 127];
            }

        } else {
            foreach($this->arPalette as $item){
                if( $item[0] <= $ceil_r ){
                    $paletteItem = $item[0];
                }
            }
            return [$paletteItem, $paletteItem, $paletteItem, 0];
        }
    }

    // показать изображение
    private function showImg($jsonFileName, $newImageName, $nPointH, $nCellH, $nCellW, $nPointW, $w, $h, $qPixXPoint, $qPixYPoint, $arColorsNew, $typeColor, $palette, $arPaletteFullColor)
    {
        $imgNew2 = imageCreate($w, $h);
        if ($this->cutOutFlow) {
            imageAlphaBlending($imgNew2, true);
            imageSaveAlpha($imgNew2, true);
        }
        // fill whole image with black background
        $black = imagecolorallocate($imgNew2, 0, 0, 0);
        imagefill($imgNew2, 0, 0, $black);

        $arColorsBox = [];

        for( $i = 0; $i < $nPointH*$nCellH; $i++ ) {
            $arColorsBox[$i] = [];
            for( $j = 0; $j < $nCellW*$nPointW; $j++) {
                $arColorsBox[$i][$j] = $this->getPixelImg($i, $j, $qPixXPoint, $qPixYPoint, $arColorsNew, $typeColor);
                $ellipseColor = imagecolorresolvealpha($imgNew2, $arColorsBox[$i][$j][0], $arColorsBox[$i][$j][1], $arColorsBox[$i][$j][2], $arColorsBox[$i][$j][3]);
                imagefilledellipse($imgNew2, ($j + 0.5)*$qPixXPoint, ($i + 0.5)*$qPixYPoint, $qPixXPoint, $qPixYPoint, $ellipseColor);
            }
        }

        // обратное отражение изображения в фронте
        if ( $this->grid_size == 71 ) {
            if ($this->cutOutFlow) {
                imagepng($imgNew2, "../upload/" . $newImageName. "_mirrored.png");
            } else {
                imagejpeg($imgNew2, "../upload/" . $newImageName. "_mirrored.jpg");
            }
            imageflip($imgNew2, IMG_FLIP_HORIZONTAL);
        }
        if ($this->cutOutFlow) {
            imagepng($imgNew2, "../upload/$newImageName.png");
        } else {
            imagejpeg($imgNew2, "../upload/$newImageName.jpg");
        }
        imageDestroy($imgNew2);

        $inp = file_get_contents("imagesJson/$jsonFileName.json");
        $tempArray = json_decode($inp, true);
        $tempArray[$newImageName]['image'] = $arColorsBox;
        $tempArray[$newImageName]['palitra'] = $palette;
        $tempArray[$newImageName]['palitraColor'] = $arPaletteFullColor;

        $jsonData = json_encode($tempArray);
        file_put_contents("imagesJson/$jsonFileName.json", $jsonData);

        return $newImageName.$this->uploadExtension;
    }

    public static function createTransparentPng(string $nameImg)
    {
        $imgPng = imagecreatefrompng($nameImg);
        imageAlphaBlending($imgPng, true);
        imageSaveAlpha($imgPng, true);
        return $imgPng;
    }

    // создание палитры цветов для каждого изображения
    private function getPaletteFull($threshold, $typeColor) {
        $arPaletteFull = [];
        $arPaletteFullColor = [];
        for($i = 0; $i < count($this->arPalette); $i++ ) {
            $num = ceil($this->arPalette[$i][0] + $this->arPalette[$i][1] + $this->arPalette[$i][2])/3;
            $arPaletteFull[$i] = [
                'color' => [
                    'r' => $num,
                    'g' => $num,
                    'b' => $num,
                ],
                'treshold' => $num + $threshold,
            ];
            if( $typeColor === 'fullcolor' ){
                $num = ceil($this->arPaletteColor[$i][0] + $this->arPaletteColor[$i][1] + $this->arPaletteColor[$i][2])/3;
                $arPaletteFullColor[$i] = [
                    'color' => [
                        'r' => $this->arPaletteColor[$i][0],
                        'g' => $this->arPaletteColor[$i][1],
                        'b' => $this->arPaletteColor[$i][2],
                    ],
                    'treshold' => $num + $threshold,
                ];
            }

        }

        if( $typeColor === 'fullcolor' ){
            return [
                $arPaletteFull,
                $arPaletteFullColor
            ];
        }else{
            return $arPaletteFull;
        }
    }

    // получение основного пиксельного изображения
    private function getPicOne($w, $h, $im, $arColorsNew, $arPalitraFull){
        $arColors = [];
        for( $i = 0; $i < $w; $i++ ) {
            $arColors[$i] = [];
            for( $j = 0; $j < $h; $j++ ) {
                $arColors[$i][$j] = imagecolorat($im, $i, $j);
                // get alpha for cutout flow. [0,127] where 0 is opaque and 127 transparent
                $alpha = $this->cutOutFlow ? ($arColors[$i][$j] & 0x7F000000) >> 24 : 0;

                $r = ($arColors[$i][$j] >> 16) & 0xFF;
                $g = ($arColors[$i][$j] >> 8) & 0xFF;
                $b = $arColors[$i][$j] & 0xFF;
                /// Ниже осуществлены старые пороги. Новые пороги можно попробовать сделать через повышение контрастности изображения
                ///
                ///
                /*$num = ceil(($r+$g+$b)/3);
                $arColorsNew[$i][$j] = $num;
                $r = $g = $b = $num;

                for ($k = 0; $k < count($arPalitraFull); $k++ ){
                    if( $num <= $arPalitraFull[$k]['treshold'] && $k <= 0 ){
                        $r = $arPalitraFull[$k]['color']['r'];
                        $g = $arPalitraFull[$k]['color']['g'];
                        $b = $arPalitraFull[$k]['color']['b'];

                        $num = ceil(($r+$g+$b)/3);
                        $arColorsNew[$i][$j] = $num;
                    }else if( $k > 0 ){
                        if( $num <= $arPalitraFull[$k]['treshold'] && $num > $arPalitraFull[$k-1]['treshold'] ){
                            $r = $arPalitraFull[$k]['color']['r'];
                            $g = $arPalitraFull[$k]['color']['g'];
                            $b = $arPalitraFull[$k]['color']['b'];

                            $num = ceil(($r+$g+$b)/3);
                            $arColorsNew[$i][$j] = $num;
                        }
                    }
                }*/
                $color = imagecolorallocatealpha($im, $r, $g, $b, $alpha);
                imagesetpixel($im, $i, $j, $color);
            }
        }

        return [
            'im' => $im,
            'colors' => $arColors
        ];
    }

    private function luminanceDistance($color_a, $color_b) {
        return (int) abs(
            0.2126 * ($color_a[0]-$color_b[0])
            + 0.7152 * ($color_a[1]-$color_b[1])
            + 0.0722 * ($color_a[2]-$color_b[2])
        );
    }

    private function createPdf($arColorsBox, $imageName, $grid_size)
    {
        $pdf = new tFPDF();

        $pdfWidth = $pdf->GetPageWidth();
        $pdfHeight = $pdf->GetPageHeight();

        $pdf->AddPage();
        $pdf->Image('../pdf/gray.jpeg', 0, 0, $pdfWidth, $pdfHeight);
        $pdf->Image('../pdf/page_1.png', 10, 6, $pdfWidth - 22, $pdfHeight - 41.5);

        $pdf->AddPage();
        $pdf->Image('../pdf/gray.jpeg', 0, 0, $pdfWidth, $pdfHeight);
        $pdf->Image('../pdf/page_2.png', 10, 6, $pdfWidth - 22, $pdfHeight - 41.5);

        $pdf->AddPage();
        $pdf->Image('../pdf/gray.jpeg', 0, 0, $pdfWidth, $pdfHeight);
        $pdf->Image('../pdf/page_3.png', 10, 6, $pdfWidth - 22, $pdfHeight - 41.5);

        $pdf->AddPage();
        $pdf->Image('../pdf/gray.jpeg', 0, 0, $pdfWidth, $pdfHeight);
        $pdf->Image('../pdf/page_4.png', 10, 6, $pdfWidth - 22, $pdfHeight - 41.5);

        $pdf->AddPage();
        $pdf->Image('../pdf/gray.jpeg', 0, 0, $pdfWidth, $pdfHeight );

        $nameImg = $grid_size == 71
            // обратное отражение изображения в PDF
            ? "../upload/" . $imageName . "_mirrored".$this->uploadExtension
            : "../upload/$imageName".$this->uploadExtension;

        $pdf->Image($nameImg, 10, 6, $pdfWidth - 22, $pdfHeight - 41.5);

        if ( $grid_size == 7 ) {
            $pdf->Image('../pdf/grid7.png', 7, 5, $pdfWidth - 17.5, $pdfHeight - 39);
        } elseif ( $grid_size == 6 ) {
            $pdf->Image('../pdf/grid6.png', 10, 6, $pdfWidth - 22, $pdfHeight - 41.3);
        } elseif ( $grid_size == 71 ) {
            $pdf->Image('../pdf/grid71.png', 8.5, 5.9, $pdfWidth - 19, $pdfHeight - 41.2 );
        } elseif ( $grid_size == 5 ) {
            $pdf->Image('../pdf/grid5.png', 8.5, 5.5, $pdfWidth - 19.5, $pdfHeight - 39.5);
        } else {
            $pdf->Image('../pdf/grid.png', 9, 6, $pdfWidth - 19.5, $pdfHeight - 40);
        }

        switch ($grid_size) {
            case 10:
                $textRatio = 0.49;
                $circleRatio = 0.496;
                break;
            case 71:
                $textRatio = 0.67;
                $circleRatio = 0.676;
                break;
            default:
                $textRatio = 0.59;
                $circleRatio = 0.596;
                break;
        }

        foreach ($this->arPaletteColor as $i => $colorDesc) {
            // Пересортировка
            $j = -1; // impossible value
            if (  $grid_size == 10 ) {
                switch ($i) {
                    case 0: $j = 9; break;
                    case 1: $j = 2; break;
                    case 2: $j = 1; break;
                    case 3: $j = 6; break;
                    case 4: $j = 7; break;
                    case 5: $j = 5; break;
                    case 6: $j = 4; break;
                    case 7: $j = 3; break;
                    case 8: $j = 8; break;
                    case 9: $j = 0; break;
                }
            } elseif ( $grid_size == 71 ) {
                switch ($i) {
                    case 0: $j = 1; break;
                    case 1: $j = 4; break;
                    case 2: $j = 3; break;
                    case 3: $j = 2; break;
                    case 4: $j = 5; break;
                    case 5: $j = 0; break;
                }
            } else {
                switch ($i) {
                    case 0: $j = 7; break;
                    case 1: $j = 2; break;
                    case 2: $j = 1; break;
                    case 3: $j = 5; break;
                    case 4: $j = 6; break;
                    case 5: $j = 4; break;
                    case 6: $j = 3; break;
                    case 7: $j = 0; break;
                }
            }
            $pdf->SetFillColor($this->arPaletteColor[$j][0], $this->arPaletteColor[$j][1], $this->arPaletteColor[$j][2]);
            $pdf->Circle($pdfWidth * $circleRatio + ($i*10),271, 4.5, 'F');

            $pdf->SetFont('Arial', '', 10);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Text($pdfWidth * $textRatio + ($i*10), 280, $this->arPaletteNumber[$j]);

            $this->colors[$colorDesc['color']['r'] + $colorDesc['color']['g'] + $colorDesc['color']['b']] = $i;
        }

        $pdf->AddPage();
        $pdf->Image('../pdf/gray.jpeg', 0, 0, $pdfWidth, $pdfHeight);

        $from = 1;
        $to = 9;
        $to_blocks = 9;
        if ( $grid_size == 71 )  {
            $to = 8;
            $to_blocks = 8;
        }

        $pdf->SetFont('Arial', 'B', 30);
        $pdf->Text($pdfWidth*80/100, 25, $from.'-'.$to);
        $pdf->Image('../pdf/counter.jpg', $pdfWidth*88/100, 16.3, 7, 10);

        $xCount = 9 * $grid_size; // Количество пикселей в одной горизотальной строке
        $yCount = 12 * $grid_size;  // Количество пикселей в одной вертикальной строке
        $last_page = 11; // Количество страниц инструкции по сборке

        if ( $grid_size == 71 )  {
            $xCount = 8 * 7;
            $yCount = 11 * 7;
            $last_page = 10;
        }

        $pixels_in_block = $grid_size; // Размер блока
        if ( $grid_size == 71 )  {
            $pixels_in_block = 7;
        }

        $width = 0;
        $height = 0;
        $newLineC = 1;
        $newPageC = 1;
        $cX = 0;
        $cY = 0;
        $numPage = 0;

        for ($blockI = 0; $blockI < $xCount/$pixels_in_block * $yCount/$pixels_in_block; $blockI++) {
            for( $x = 0, $j = $cX; $x < $pixels_in_block, $j < $cX + $pixels_in_block; $x++, $j++) {
                for( $y = 0, $yPos = $cY * $pixels_in_block; $y < $pixels_in_block, $yPos < $cY * $pixels_in_block + $pixels_in_block; $y++, $yPos++ ) {

                    $currentColorName = $arColorsBox[$j][$y][0] + $arColorsBox[$j][$y][1] + $arColorsBox[$j][$y][2];

                    if (!isset($this->colors[$currentColorName])) {
                        $checkColorExists = [
                            'index' => null,
                            'diff' => null,
                            'name' => null
                        ];

                        foreach ($this->colors as $index => $colorNames) {
                            $diff = $currentColorName - $index;
                            $diff = $diff < 0 ? (-1) * $diff : $diff;
                            if (is_null($checkColorExists['name']) || $checkColorExists['diff'] > $diff) {
                                $checkColorExists['index'] = $index;
                                $checkColorExists['name'] = $colorNames;
                                $checkColorExists['diff'] = $diff;
                            }
                        }
                    }

                    $textColorName = '';
                    $r = $arColorsBox[$j][$yPos][0];
                    $g = $arColorsBox[$j][$yPos][1];
                    $b = $arColorsBox[$j][$yPos][2];
                    $alpha = $arColorsBox[$j][$yPos][3];
                    foreach ($this->arPaletteNumber as $key => $item ) {
                        if ( $this->arPaletteColor[$key][0] == $r) {
                            $textColorName = $item;
                            break;
                        }
                    }

                    $magnification = 6.2;
                    $font_shift = 1.2;
                    $radius = 3;
                    $font_size = 10;

                    if ( $grid_size == 10 ) {
                        $magnification = 4.2;
                        $font_shift = 1;
                        $radius = 2;
                        $font_size = 8;
                    }

                    // transparent blocks will be rendered as white circle without fill
                    if ($alpha === 127) {
                        $pdf->SetDrawColor(255, 255, 255);
                        $pdf->SetFillColor(255, 255, 255);
                        $pdf->Circle($pdfWidth*10/100 + ($y*$magnification) + $width, $pdfHeight*14/100 + ($x*$magnification) + $height, $radius, 'C');
                    } else {
                        $pdf->SetFillColor($r, $g, $b);
                        $pdf->Circle($pdfWidth*10/100 + ($y*$magnification) + $width, $pdfHeight*14/100 + ($x*$magnification) + $height, $radius, 'F');

                        $pdf->SetFont('Arial', '', $font_size);
                        $pdf->Text($pdfWidth*10/100 + ($y*$magnification) + $width - $font_shift, $pdfHeight*14/100 + ($x*$magnification) + $height + $font_shift, $textColorName);
                    }
                }
            }

            if ( $grid_size == 71 && $numPage == 10 ) {
                $pdf->Image('../pdf/logo_blue3.png', 24.2, 57, 38, 18.4);
            }
            $pdf->SetFont('Arial', '', 15);
            $pdf->Text($pdfWidth*4/100 + $width, $pdfHeight*15.2/100 + $height, $blockI+1);

            $width+=60;
            $cY++;

            if ($newPageC % 3 == 0) {
                $height+=80;
                $width = 0;
            }

            if ($newLineC % $to_blocks == 0) {
                $cX += $pixels_in_block;
                $cY = 0;
                $newLineC = 0;
            }

            if ($newPageC % $to_blocks == 0 && $numPage < $last_page) {
                $pdf->AddPage();

                $pdf->Image('../pdf/gray.jpeg', 0, 0, $pdfWidth, $pdfHeight);
                //$pdf->Image('../pdf/logo.jpg', 10, 15, 75, 6);

                $from += $to_blocks;
                $to += $to_blocks;

                $pdf->SetFont('Arial', 'B', 30);
                if ( $numPage > 9 ) {
                    $pdf->Text($pdfWidth*74/100, 25, $from.'-'.$to);
                } else {
                    $pdf->Text($pdfWidth*80/100, 25, $from.'-'.$to);
                }
                $pdf->Image('../pdf/counter.jpg', $pdfWidth*93/100, 16.3, 7, 10);


                $height = 0;
                $width = 0;
                $newPageC = 0;
                $numPage ++;
            }

            $newLineC++;
            $newPageC++;
        }

        $pdf->Output('F', "../upload/pdf/zufapixel_guide.pdf");

        return "zufapixel_guide.pdf";
    }

    public function sendEmail($data)
    {
        $config = config();
        $secretCodeUsageCount = $config['secret_code_usage_count'];
        $this->analyzeUploadData($data);

        $databaseManager = new DatabaseManager();
        $connection = $databaseManager->connect();

        $stmt = $connection->prepare("SELECT * FROM secret_codes WHERE id_secret=?");
        $stmt->execute([$data['code']]);
        $secretCode = $stmt->fetch();

        $grid_size = (int)$data['grid_size'];

        if ( $grid_size < 10 ) {
            $this->arPaletteColor = [
                self::COLOR_J_CYAN, //J
                self::COLOR_C_MAGENTA, //C
                self::COLOR_B_VIOLET, //B
                self::COLOR_H_GREEN, //H
                self::COLOR_G_RED, //G
                //F
                self::COLOR_D_YELLOW, //A - D
                self::COLOR_E_CORAL, //E
                //I
                self::COLOR_A_LILAC //D -A
            ];
            $this->arPaletteNumber = [ 'J', 'C', 'B', 'H', 'G', 'D', 'E', 'A'];
        }  elseif ( $grid_size == 71 ) {
            $this->arPaletteColor = [
                self::COLOR_J_CYAN, //J
                self::COLOR_C_MAGENTA, //C
                //B
                self::COLOR_H_GREEN, //H
                self::COLOR_G_RED, //G
                //F
                //A
                self::COLOR_E_CORAL,//E
                self::COLOR_I_LIGHTBLUE, //I
                //D
            ];
            $this->arPaletteNumber = [ 'J', 'C', 'H', 'G', 'E', 'I'];
        } else {
            $this->arPaletteColor = [
                self::COLOR_J_CYAN, //J
                self::COLOR_C_MAGENTA, //C
                self::COLOR_B_VIOLET, //B
                self::COLOR_H_GREEN, //H
                self::COLOR_G_RED, //G
                self::COLOR_F_BROWN, //F
                self::COLOR_D_YELLOW, //A - D
                self::COLOR_E_CORAL, //E
                self::COLOR_I_LIGHTBLUE, //I
                self::COLOR_A_LILAC, //D - A
            ];
            $this->arPaletteNumber = [ 'J', 'C', 'B', 'H', 'G', 'F', 'D', 'E', 'I', 'A' ];
        }

        if (!$secretCode) {
            http_response_code(404);

            return [
                'success' => false,
                'text' => 'Секретный код не существует.'
            ];
        }

        $valueSecret = $secretCode['value_secret'];

        if ($valueSecret >= $secretCodeUsageCount) {
            http_response_code(404);

            return [
                'success' => false,
                'text' => "Один и тот же код можно использовать не более $secretCodeUsageCount раз."
            ];
        }

        if ( $data['code'] != 56565656  ) {
            $sql = "UPDATE secret_codes SET value_secret=? WHERE id_secret=?";
            $stmt= $connection->prepare($sql);
            $stmt->execute([$valueSecret+1, $data['code']]);
        }
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('Europe/Moscow'));
        $date_val = $date->format('Y-m-d H:i:s');
        $sql = "INSERT INTO email VALUES (DEFAULT, ?, ?)";
        $stmt = $connection->prepare($sql);
        $stmt->execute([$data['email'],$date_val]);

        $imageName = str_replace(['.jpg', '.png'], '', str_replace($config['app_url'].'/upload/', '', $data['pic']));

        $jsonFileName = substr($imageName, 0, strpos($imageName, "_new")).'.json';

        if (!file_exists("imagesJson/$jsonFileName")) {
            http_response_code(400);

            return [
                'success' => false,
                'text' => "Недействительное изображение."
            ];
        }

        $inp = file_get_contents("imagesJson/$jsonFileName");
        $tempArray = json_decode($inp, true);

        if (!isset($tempArray[$imageName])) {
            http_response_code(400);

            return [
                'success' => false,
                'text' => "Недействительное изображение."
            ];
        }

        $pdf = $this->createPdf($tempArray[$imageName]['image'], $imageName, $grid_size);

        $html = "<p>Схема для раскрашивания во вложении.</p> </ br></ br>".
            "<p>Желаем приятно провести время!</p>".
            "<p>Выкладывайте и отмечайте в социальных сетях #zufapixel @zufapixel, тогда мы выложим вашу работу на своей странице.</p>".
            "<p>Ваш код товара: " . $data['code'] . "</p>".
            "<p>Используя этот код вы сможете воспользоваться программой для преобразования фото ограниченное количество раз: " . $secretCodeUsageCount . "</p>".
            "<p>При возникновении вопросов, ищите ответы в частых вопросах <a href='https://zufapixel.ru/faq'>zufapixel.ru/faq</a></p>";

        sendMail($config['smtp'], $data['email'], 'Схема для раскрашивания', $html, "../upload/pdf/$pdf");

        unlink("imagesJson/$jsonFileName");

        return [
            'success' => true,
            'file' => config()['app_url']."/upload/pdf/$pdf",
            'text' => 'Успешно отправлено',
            'codeNum' => $secretCodeUsageCount - ($valueSecret + 1)
        ];
    }
}
