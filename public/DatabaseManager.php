<?php

class DatabaseManager {
    private $serverName;

    private $userName;

    private $password;

    private $database;

    public function __construct()
    {
        $config = config();

        $this->serverName = $config['db']['server_name'];
        $this->userName = $config['db']['user_name'];
        $this->password = $config['db']['password'];
        $this->database = $config['db']['database'];

    }
    public function connect()
    {
        $conn = new PDO("mysql:host=$this->serverName;dbname=$this->database", $this->userName, $this->password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $conn;
    }
}