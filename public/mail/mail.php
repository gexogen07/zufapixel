<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as PHPMailerException;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

function sendErrorMail($smtpConfig, $html) {
    try {
        $mail = new PHPMailer(true);

        $mail->CharSet = 'UTF-8';
        $mail->IsSmtp();
        $mail->Host = $smtpConfig['host'];
        $mail->Port = $smtpConfig['port'];
        $mail->Username = $smtpConfig['email'];
        $mail->Password = $smtpConfig['password'];
        $mail->Mailer = 'smtp';
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->setFrom($smtpConfig['email'], '=?UTF-8?B?'.base64_encode($smtpConfig['admin']).'?=');
        $mail->addAddress($smtpConfig['email']);
        $mail->Subject = '=?UTF-8?B?'.base64_encode("Ошибка в работе CutOut API").'?=';
        $mail->Body = $html;
        $mail->Timeout =   60;
        $mail->SMTPKeepAlive = true;

        $mail->send();

    } catch (PHPMailerException $e) {
        $logFile = fopen("logs.txt", "x");
        // we can't even log exception
        if ($logFile !== false) {
            $txt = $mail->ErrorInfo . "\n
    ";
            fwrite($logFile, $txt);
            $txt = $e->getMessage() . "\n
    ";
            fwrite($logFile, $txt);
            fclose($logFile);
        }
    }
}

function sendMail($smtpConfig, $to, $subject, $html, $file)
{
    try {
        $mail = new PHPMailer(true);

        $mail->charSet = 'UTF-8';
        $mail->IsSmtp();
        $mail->Host = $smtpConfig['host'];
        $mail->Port = $smtpConfig['port'];
        $mail->Username = $smtpConfig['email'];
        $mail->Password = $smtpConfig['password'];
        $mail->Mailer = 'smtp';
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->AddAttachment(
            $file,
            'zufapixel-manual.pdf',
            'base64',
            'mime/type'
        );

        $mail->setFrom($smtpConfig['email'], '=?UTF-8?B?'.base64_encode($smtpConfig['admin']).'?=');
        $mail->addAddress($to);
        $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
        $mail->IsHTML();
        $mail->Body = $html;
        $mail->Timeout = 60;
        $mail->SMTPKeepAlive = true;

        /*$mail->Debugoutput = function($str, $level) {
            $logFile = fopen("logs.txt", "x");
            $txt = "debug level $level; message: $str";
            fwrite($logFile, $txt);
            fclose($logFile);
        };*/

        $mail->send();

    } catch (PHPMailerException $e) {
        $logFile = fopen("logs.txt", "x");
        $txt = $mail->ErrorInfo."\n
";
        //$txt .= $mail->smtpConnect()."\n";
        fwrite($logFile, $txt);
        $txt = $e->getMessage()."\n
";
        fwrite($logFile, $txt);
        fclose($logFile);
    }
}